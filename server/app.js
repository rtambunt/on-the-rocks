const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");

const userRouter = require("./routes/userRoutes");
const globalErrorHandler = require("./controllers/errorController");
const { storage } = require("./utils/gcloudStorage");

const app = express();

// CORS
const corsOptions = {
  origin: [
    `http://127.0.0.1:${process.env.FRONTEND_PORT}`,
    `http://localhost:${process.env.FRONTEND_PORT}`,
    `${process.env.FRONTEND_URL}`,
    `https://on-the-rocks.vercel.app`,
    `https://on-the-rocks-git-main-robbies-projects.vercel.app`,
    `https://on-the-rocks-8sspr8g8s-robbies-projects.vercel.app`,
    `https://ontherocksapp.com`,
    `https://www.ontherocksapp.com`
  ], // Only allow requests from these origins
  methods: "GET,POST,PUT,PATCH,DELETE,OPTIONS",
  allowedHeaders: "Content-Type,Authorization",
  credentials: true, // Allow sending cookies and credentials
};

app.use(cors(corsOptions));
app.options("*", cors(corsOptions));

app.use(cookieParser()); // parses cookies
app.use(express.json());

// Route Mounting
app.use("/api/users", userRouter);

// Global Error Handler
app.use(globalErrorHandler);

module.exports = app;
