const express = require("express");

const drinkController = require("../controllers/drinkController");
const { protect } = require("../controllers/authController");
const { uploadSingle } = require("../utils/gcloudStorage");

const router = express.Router();
const { getDrinks, getDrinkById, createDrink, updateDrink, deleteDrink } =
  drinkController;

router
  .route("/")
  .get(protect, getDrinks)
  .post(
    protect,
    (req, res, next) => {
      uploadSingle(req, res, (err) => {
        if (err) {
          console.log("Error in upload middleware", err);
          return res.status(400).send({ error: err.message });
        }

        next();
      });
    },
    createDrink
  );
router
  .route("/:drinkId")
  .get(protect, getDrinkById)
  .patch(
    protect,
    (req, res, next) => {
      uploadSingle(req, res, (err) => {
        if (err) {
          console.log("Error in upload middleware", err);
          return res.status(400).send({ error: err.message });
        }
        next();
      });
    },
    updateDrink
  )
  .delete(protect, deleteDrink);

module.exports = router;
