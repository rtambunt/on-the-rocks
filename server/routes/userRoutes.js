const express = require("express");

const {
  signUp,
  login,
  protect,
  logout,
  isLoggedIn,
  forgotPassword,
  resetPassword,
  updatePassword,
  verifyEmail,
  resendEmailVerification,
} = require("../controllers/authController");
const {
  getUsers,
  getUserById,
  getMe,
  updateMe,
  deleteMe,
} = require("../controllers/userController");
const drinkRouter = require("./drinkRoutes");

const router = express.Router();

// --- Auth Routes ---

router.post("/sign-up", signUp);
router.post("/login", login);
router.get("/logout", logout);
router.get("/is-logged-in", isLoggedIn);

router.post("/verify-email/:verifyToken", verifyEmail);
router.post("/forgot-password", forgotPassword);
router.patch("/reset-password/:token", resetPassword);
router.post("/resend-email-verification", resendEmailVerification);
router.patch("/update-password", protect, updatePassword);

// ! - These user routes are for admin use only - (TO DO: Restrict Access)
router.route("/").get(getUsers);

// --- Access Routes ---

// - /me allows access to logged-in users data (including drinks)
// /me acts as a /:userId endpoint
router.route("/me").get(protect, getMe, getUserById);
router.use("/me/drinks", drinkRouter); // Mount drinkRouter
router.patch("/update-me", protect, updateMe);
router.delete("/delete-me", protect, deleteMe);

module.exports = router;
