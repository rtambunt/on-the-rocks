const mongoose = require("mongoose");
const dotenv = require("dotenv");

// --- Uncaught Exception Handler Safety Net ---
process.on("uncaughtException", (err) => {
  console.log(err.name, err.message);
  console.log("Unhandled Exception! Shutting down...");

  process.exit(1);
});

dotenv.config({ path: "./config.env" });
const app = require("./app");

const DB_STRING = process.env.DB_STRING.replace("<PW>", process.env.DB_PW);

mongoose.connect(DB_STRING).then((con) => {
  console.log(con.connections);
  console.log("DB connection is successful!");
});

const port = process.env.PORT;
app.listen(port, process.env.IP, () => {
  console.log(`App is running on port ${port}!`);
});

process.on("unhandledRejection", (err) => {
  console.log(err.name, err.message);
  console.log("Unhandled Rejection! Shutting down...");
  server.close(() => {
    process.exit(1);
  });
});
