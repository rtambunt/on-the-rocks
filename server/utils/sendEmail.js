const sgMail = require("@sendgrid/mail");

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendEmail = async (url, emailType, toEmail, firstName) => {
  const msg = {
    to: toEmail,
    from: {
      name: "On the Rocks App",
      email: "otr-main@ontherocksapp.com",
    },
    templateId:
      emailType === "sign-up"
        ? process.env.EMAIL_VERIFY_TEMPLATE_ID
        : process.env.PW_RESET_TEMPLATE_ID,
    dynamicTemplateData: {
      firstName,
      url,
    },
  };

  try {
    await sgMail.send(msg);
  } catch (error) {
    // console.error(error);
    console.error(error.response.body);
    throw error;
  }
};

module.exports = sendEmail;
