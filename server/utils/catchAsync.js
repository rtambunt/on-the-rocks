// ** For middleware use only! **
// Returns a function with parameters req, res, next

module.exports = (fn) => {
  return (req, res, next) => fn(req, res, next).catch(next);
};
