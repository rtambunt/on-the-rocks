const path = require("path");
const multer = require("multer");
const multerGoogleStorage = require("multer-cloud-storage");
const { Storage } = require("@google-cloud/storage");

const AppError = require("./appError");
const catchAsync = require("./catchAsync");

const storage = new Storage({
  projectId: process.env.GCLOUD_PROJECT_ID,
  bucket: process.env.GCLOUD_BUCKET_NAME,
  keyFilename: path.join(__dirname, "..", process.env.GCLOUD_KEYFILE_PATH),
  filename: (req, file, cb) => {
    const ext = file.mimetype.split("/")[1];
    cb(null, `${req.user.id}-${Date.now()}.${ext}`);
  },
});

const deleteStorageObj = catchAsync(async (fileName) => {
  await storage.bucket(process.env.GCLOUD_BUCKET_NAME).file(fileName).delete();
});

const multerStorage = multerGoogleStorage.storageEngine({
  bucket: process.env.GCLOUD_BUCKET_NAME,
  projectId: process.env.GCLOUD_PROJECT_ID,
  keyFilename: path.join(__dirname, "..", process.env.GCLOUD_KEYFILE_PATH),
  filename: (req, file, cb) => {
    const ext = file.mimetype.split("/")[1];
    cb(null, `${req.user.id}-${Date.now()}.${ext}`);
  },
});

const imageFileFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) cb(null, true);
  else
    cb(
      new AppError("Not an image! Please upload only image files", 400),
      false
    );
};

const uploadSingle = multer({
  storage: multerStorage,
  fileFilter: imageFileFilter,
}).single("image");

const baseStorageUrl = "https://storage.googleapis.com/on-the-rocks-bucket/";

module.exports = { uploadSingle, deleteStorageObj, baseStorageUrl };
