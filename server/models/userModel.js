const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcrypt");
const crypto = require("crypto");

const userSchema = mongoose.Schema(
  {
    firstName: {
      type: String,
      required: [true, "First names are required"],
      maxLength: [20, "First names have a max length of 20"],
    },
    lastName: {
      type: String,
      maxLength: [30, "Last names have a max length of 30"],
    },
    email: {
      type: String,
      unique: true,
      lowercase: true,
      required: [true, "An email address is required!"],
      validate: [validator.isEmail, "Please enter a valid email address"],
    },
    image: String,
    password: {
      type: String,
      minLength: [8, "Please enter 8 or more characters for your password"],
      required: [true, "Please enter a password"],
      select: false,
    },
    passwordConfirm: {
      type: String,
      validate: {
        validator: function (pwConfirm) {
          return this.password === pwConfirm;
        },
        message: "Password and password confirmation must match!",
      },
      required: [true, "Please enter a password"],
      select: false,
    },
    emailVerifyToken: String,
    emailVerifyExpiresAt: Date,
    isVerified: {
      type: Boolean,
      default: false,
    },
    passwordChangedAt: Date,
    passwordResetToken: String,
    passwordResetExpiresAt: Date,
  },
  { toJSON: { virtuals: true }, toObject: { virtuals: true } }
);

// -- Virtual Properties --
// Virtual Populate with user drinks
userSchema.virtual("drinks", {
  ref: "Drink",
  foreignField: "user",
  localField: "_id",
});

// -- Middleware --
userSchema.pre("save", async function (next) {
  // If pw isn't changed, continue
  if (!this.isModified("password")) return next();

  // Hash pw before saving to db
  this.password = await bcrypt.hash(this.password, 12);
  this.passwordConfirm = undefined;
  next();
});

// If document pw has been changed and it isn't a new document, set passwordChangedAt timestamp
userSchema.pre("save", async function (next) {
  if (!this.isModified("password") || this.isNew) {
    return next();
  }

  this.passwordChangedAt = Date.now() - 1000;

  next();
});

// -- Instance Methods --
userSchema.methods.isCorrectPw = async function (candidatePw, userPw) {
  return await bcrypt.compare(candidatePw, userPw);
};

userSchema.methods.changedPwAfterToken = function (jwtTimestamp) {
  // Check if pw has been changed

  if (this.passwordChangedAt) {
    // iat is in seconds, convert passwordChangedAt to sec
    const pwTimestampSec = parseInt(
      this.passwordChangedAt.getTime() / 1000,
      10
    );

    return jwtTimestamp < pwTimestampSec;
  }

  // If pw never changed, return false
  return false;
};

userSchema.methods.createEmailVerifyToken = function () {
  const verifyToken = crypto.randomBytes(32).toString("hex");

  this.emailVerifyToken = crypto
    .createHash("sha256")
    .update(verifyToken)
    .digest("hex");

  this.emailVerifyExpiresAt = Date.now() + 1000 * 60 * 10;

  return verifyToken;
};

userSchema.methods.createPwResetToken = function () {
  const resetToken = crypto.randomBytes(32).toString("hex");

  this.passwordResetToken = crypto
    .createHash("sha256")
    .update(resetToken)
    .digest("hex");

  this.passwordResetExpiresAt = Date.now() + 1000 * 60 * 10; // 10min expiration

  return resetToken;
};

// -- User Model --
const User = mongoose.model("User", userSchema);
module.exports = User;
