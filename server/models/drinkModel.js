const mongoose = require("mongoose");
const ingredientSchema = require("./ingredientModel");
const instructionSchema = require("./instructionModel");

const drinkSchema = mongoose.Schema({
  name: { type: String, required: [true, "Drink name is required"] },
  description: String,
  image: String,
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: [true, "A drink must be associated with a user!"],
  },
  createdAt: { type: Date, default: Date.now() },
  ingredients: [ingredientSchema],
  instructions: [instructionSchema],
});

// -- Middleware --
drinkSchema.pre(/^find/, function (next) {
  // Populate query with user data
  this.populate({ path: "user", select: "firstName image" });

  next();
});

const Drink = mongoose.model("Drink", drinkSchema);

module.exports = Drink;
