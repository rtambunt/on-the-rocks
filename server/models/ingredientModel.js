const mongoose = require("mongoose");

const ingredientSchema = mongoose.Schema({
  name: { type: String, required: [true, "Ingredient name is required"] },
  type: String,
  quantity: Number,
  units: String,
  //  image: String,  Might implement in future
});

module.exports = ingredientSchema;
