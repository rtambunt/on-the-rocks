const mongoose = require("mongoose");

const instructionSchema = mongoose.Schema({
  instruction: {
    type: String,
    required: [true, "Instruction name is required"],
  },
  orderNum: Number,
});

module.exports = instructionSchema;
