const AppError = require("../utils/appError");

handleDBCastError = (err) => {
  const message = `${err.value} is an invalid id for path ${err.path}`;

  return new AppError(message, 400);
};

handleDBDuplicateFields = (err) => {
  const value = err.errorResponse.errmsg.match(/"(.*?)"/)[0];

  if ("email" in err.keyValue)
    return new AppError(`User account with email ${value} already exists`, 400);

  const message = `Duplicate field value: ${value}`;

  return new AppError(message, 400);
};

const handleDBValidationError = (err) => {
  const errors = Object.values(err.errors).map((el) => el.message);
  const message = `Invalid input data. ${errors.join(". ")}`;
  return new AppError(message, 400);
};

const sendDevError = (err, res) => {
  // Send all info about error during dev
  res.status(err.statusCode).json({
    status: err.status,
    err,
    message: err.message,
    stack: err.stack,
  });
};

const sendProdError = (err, res) => {
  if (err.isOperational) {
    // -- Operational Errors --
    // Predictable errors related to users using the software wrong
    // Validation errors, invalid db id's, etc..

    res
      .status(err.statusCode)
      .json({ status: err.status, message: err.message });
  } else {
    // -- Programmer Errors --
    // Unintended bugs
    console.error("ERROR!", err);
    res.status(500).json({
      status: "error",
      message: "Something went very wrong! Server error",
    });
  }
};

// Error handling middleware - contains extra arg "err"
module.exports = (err, req, res, next) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || "error";

  if (process.env.NODE_ENV === "development") {
    sendDevError(err, res);
  } else if (process.env.NODE_ENV === "production") {
    let error = { ...err };

    if (error.name === "CastError") error = handleDBCastError(error);
    if (error.code === 11000) error = handleDBDuplicateFields(error);
    if (error.name === "ValidationError")
      error = handleDBValidationError(error);

    sendProdError(error, res);
  }
};
