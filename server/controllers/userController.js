const User = require("../models/userModel");
const AppError = require("../utils/appError");
const catchAsync = require("../utils/catchAsync");

const filterObj = (obj, ...alllowedFields) => {
  const newObj = {};

  Object.keys(obj).forEach((el) => {
    if (alllowedFields.includes(el)) newObj[el] = obj[el];
  });

  return newObj;
};

exports.getMe = (req, res, next) => {
  // Prevents user.id exposure via url
  req.params.id = req.user.id;

  next();
};

exports.getUsers = catchAsync(async (req, res) => {
  const users = await User.find();

  res.status(200).json({
    status: "success",
    results: users.length,
    data: {
      users,
    },
  });
});

exports.updateMe = catchAsync(async (req, res, next) => {
  // Send error if user tries to update password
  if (req.body.password || req.body.passwordConfirm)
    return next(
      new AppError(
        "This route is not for updating your password. Please use /update-password route instead!",
        400
      )
    );

  // Filter body so user can't upload different fields

  const filteredBody = filterObj(req.body, "firstName", "lastName", "email");

  const updatedUser = await User.findByIdAndUpdate(req.user.id, filteredBody, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({ status: "success", data: { user: updatedUser } });
});

exports.getUserById = catchAsync(async (req, res, next) => {
  const user = await User.findById(req.params.id).populate("drinks");

  if (!user)
    return next(
      new AppError(`User with id ${req.params.id} does not exist!`, 404)
    );

  res.status(200).json({
    status: "success",
    data: {
      user,
    },
  });
});

exports.deleteMe = catchAsync(async (req, res, next) => {
  await User.findByIdAndDelete(req.user.id);

  res.status(204).json({
    status: "success",
    message: "Drink successfully deleted!",
    data: null,
  });
});
