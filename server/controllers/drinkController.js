const Drink = require("../models/drinkModel");
const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const { deleteStorageObj, baseStorageUrl } = require("../utils/gcloudStorage");

exports.getDrinks = catchAsync(async (req, res) => {
  const drinks = await Drink.find({ user: req.user.id });

  res.status(200).json({
    status: "success",
    results: drinks.length,
    data: { drinks },
  });
});

exports.getDrinkById = catchAsync(async (req, res, next) => {
  const drink = await Drink.findById(req.params.drinkId);

  if (!drink)
    return next(
      new AppError(`No drink found with id ${req.params.drinkId}`, 404)
    );

  // Sort drink instructions by orderNum when querying
  drink?.instructions?.sort((a, b) => a.orderNum - b.orderNum);

  res.status(200).json({
    status: "success",
    data: {
      drink,
    },
  });
});

exports.createDrink = catchAsync(async (req, res) => {
  if (!req.body.user) req.body.user = req.user.id;

  if (req.file && req.file.linkUrl) req.body.image = req.file.linkUrl;
  else req.body.image = null;
  // if (req.body.image === "undefined") req.body.image = null;

  // Parse ingredients + instructions if stringified during frontend form submit
  if (typeof req.body.ingredients === "string")
    req.body.ingredients = JSON.parse(req.body.ingredients);

  if (typeof req.body.instructions === "string")
    req.body.instructions = JSON.parse(req.body.instructions);

  const newDrink = await Drink.create(req.body);

  res.status(201).json({
    status: "success",
    data: {
      drink: newDrink,
    },
  });
});

exports.updateDrink = catchAsync(async (req, res, next) => {
  // Parse ingredients + instructions if stringified during frontend form submit
  if (typeof req.body.ingredients === "string")
    req.body.ingredients = JSON.parse(req.body.ingredients);

  if (typeof req.body.instructions === "string")
    req.body.instructions = JSON.parse(req.body.instructions);

  // If no file uploaded, don't update image field
  if (!req.file) delete req.body.image;

  const updatedDrink = await Drink.findByIdAndUpdate(
    req.params.drinkId,
    req.body,
    {
      new: true,
      runValidators: true,
    }
  );

  if (!updatedDrink)
    return next(
      new AppError(`No drink found with id ${req.params.drinkId}`, 404)
    );

  // Check if user sent in a new image file

  if (req?.file) {
    // If drink already has an image in db, delete that image from storage
    if (updatedDrink.image) {
      const drinkImageName = updatedDrink.image.split(baseStorageUrl)[1];

      deleteStorageObj(drinkImageName);
    }

    // Add new url to image
    updatedDrink.image = req.file.linkUrl;
    await updatedDrink.save();
  }

  res.status(200).json({
    status: "success",
    data: {
      drink: updatedDrink,
    },
  });
});

exports.deleteDrink = catchAsync(async (req, res, next) => {
  const deletedDrink = await Drink.findByIdAndDelete(req.params.drinkId);

  if (!deletedDrink)
    return next(
      new AppError(`No drink found with id ${req.params.drinkId}`, 404)
    );

  // Delete drink image storage object
  if (deletedDrink.image) {
    const drinkImageName = deletedDrink?.image.split(baseStorageUrl)[1];
    deleteStorageObj(drinkImageName);
  }

  res.status(204).json({
    status: "success",
    message: "Drink successfully deleted!",
    data: null,
  });
});
