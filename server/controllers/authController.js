const jwt = require("jsonwebtoken");
const { promisify } = require("util");
const crypto = require("crypto");

const User = require("../models/userModel");
const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const sendEmail = require("../utils/sendEmail");

const signToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
};

const createSendToken = (user, statusCode, res) => {
  const token = signToken(user._id);

  const cookieOptions = {
    expires: new Date(
      Date.now() + process.env.COOKIE_EXPIRES * 24 * 60 * 60 * 1000
    ),
    secure: true, // only send cookie via https
    httpOnly: true,
    sameSite: "None",
  };

  res.cookie("jwt", token, cookieOptions);

  user.password = undefined;

  res.status(statusCode).json({
    status: "success",
    token,
    data: { user },
  });
};

exports.signUp = catchAsync(async (req, res, next) => {
  // Create new user
  // Handle pw encryption in pre-save middleware
  const newUser = await User.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    image: req.body.image,
    password: req.body.password,
    passwordConfirm: req.body.passwordConfirm,
  });

  try {
    // Generate email verify token
    const verifyToken = newUser.createEmailVerifyToken();

    const verifyUrl = `${process.env.FRONTEND_URL}/verify-email/${verifyToken}`;

    // Send Verification Email
    await sendEmail(verifyUrl, "sign-up", req.body.email, req.body.firstName);

    await newUser.save({ validateBeforeSave: false });

    res.status(201).json({
      status: "success",
      message: "User created! Please check your email to activate your account",
    });
  } catch (err) {
    // If email could not be sent, delete stored user data
    await User.findByIdAndDelete(newUser._id);

    return next(
      new AppError(
        "There was an error sending your account verification email. Please try again later!",
        500
      )
    );
  }
});

exports.resendEmailVerification = catchAsync(async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });

  if (!user)
    return next(new AppError("No user exists with that email. Please sign up"));

  try {
    const verifyToken = user.createEmailVerifyToken();

    const verifyUrl = `${process.env.FRONTEND_URL}/verify-email/${verifyToken}`;

    await sendEmail(verifyUrl, "sign-up", user.email, user.firstName);

    await user.save({ validateBeforeSave: false });

    res.status(200).json({
      status: "success",
      message: "Verification email resent!",
    });
  } catch (err) {
    console.error(err);
    return next(
      new AppError(
        "There was an error re-sending your account verification email. Please try again later!",
        500
      )
    );
  }
});

exports.verifyEmail = catchAsync(async (req, res, next) => {
  // Hash token (Should be same as what's stored in db)
  const hashedToken = crypto
    .createHash("sha256")
    .update(req.params.verifyToken)
    .digest("hex");

  // Get user based on token and check if token is expired
  const user = await User.findOne({
    emailVerifyToken: hashedToken,
    emailVerifyExpiresAt: { $gt: Date.now() },
  });

  if (!user)
    return next(
      new AppError(
        "Token is invalid or has expired! Please try again or request a new url link",
        400
      )
    );

  // Activate account
  user.isVerified = true;
  user.emailVerifyToken = undefined;
  user.emailVerifyExpiresAt = undefined;

  await user.save();

  res.status(200).json({
    status: "success",
    message:
      "Email has been verified and account is activated! You can now log in",
  });
});

exports.login = catchAsync(async (req, res, next) => {
  // Check if email + password were entered
  const { email, password } = req.body;
  if (!email || !password)
    return next(
      new AppError("Please provide both your email and password"),
      400
    );

  // Get user by email + include pw in query
  const user = await User.findOne({ email }).select("+password");

  //   Check if user was found
  if (!user)
    return next(new AppError("User with that email was not found!", 404));

  //   Check if pw's match. Offload pw comparing to instance method in User model
  if (!(await user.isCorrectPw(password, user.password)))
    return next(new AppError("Incorrect password. Please try again", 401));

  if (!user.isVerified)
    return next(
      new AppError(
        "Your account is not verified! Please request a verification link to verify your email",
        403
      )
    );

  // Sign jwt
  createSendToken(user, 200, res);
});

exports.logout = (req, res) => {
  res.cookie("jwt", "loggedout", {
    expires: new Date(Date.now() + 10 + 1000), // Expires in 10 sec
    httpOnly: true,
    secure: true,
    sameSite: "None",
  });

  res.status(200).json({ status: "success" });
};

exports.isLoggedIn = async (req, res) => {
  let token;
  // Get token + check if it exists
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  } else if (req.cookies.jwt) {
    token = req.cookies.jwt;
  }

  if (!token) return res.status(200).json({ isLoggedIn: false });

  try {
    await promisify(jwt.verify)(token, process.env.JWT_SECRET);
    res.status(200).json({ isLoggedIn: true });
  } catch (err) {
    res.status(200).json({ isLoggedIn: false });
  }
};

exports.protect = catchAsync(async (req, res, next) => {
  let token;
  // Get token + check if it exists

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  } else if (req.cookies.jwt) {
    token = req.cookies.jwt;
  }

  if (!token) return next(new AppError("Please log in to get access!", 401));

  // Verify token

  const decodedToken = await promisify(jwt.verify)(
    token,
    process.env.JWT_SECRET
  ); // jwt.verify into a promise to avoid synchronous version

  // Check if user exists
  const curUser = await User.findById(decodedToken.id);

  if (!curUser)
    return next(
      new AppError("The user belonging to this token no longer exists", 401)
    );

  // Check if user changed pw after token is issued
  // Instance method "changedPwAFterToken" used to handle logic
  if (curUser.changedPwAfterToken(decodedToken.iat))
    return next(
      new AppError(
        "Password has been changed recently. Please log in again!",
        401
      )
    );

  // Grant access to route
  // Assign user to request object
  req.user = curUser;

  next();
});

exports.forgotPassword = catchAsync(async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });

  if (!user) return next(new AppError("No user found with that email!", 404));

  // Generate random token
  const resetToken = user.createPwResetToken();
  await user.save({ validateBeforeSave: false });

  const resetUrl = `${process.env.FRONTEND_URL}/reset-password/${resetToken}`;
  // const resetUrl = `${req.protocol}://${req.get("host")}/api/users/reset-password/${resetToken}`;

  // Send Email
  try {
    await sendEmail(resetUrl, "pw-reset", req.body.email, user.firstName);

    res.status(200).json({
      status: "success",
      message: "Verification link has been sent to email!",
    });
  } catch (err) {
    user.passwordResetToken = undefined;
    user.passwordResetExpiresAt = undefined;

    await user.save({ validateBeforeSave: false });

    return next(
      new AppError(
        "There was an error sending the password reset email. Please try again later!",
        500
      )
    );
  }
});

exports.resetPassword = catchAsync(async (req, res, next) => {
  // Get user based on token
  const hashedToken = crypto
    .createHash("sha256")
    .update(req.params.token)
    .digest("hex");

  const user = await User.findOne({
    passwordResetToken: hashedToken,
    passwordResetExpiresAt: { $gt: Date.now() },
  });

  if (!user)
    return next(
      new AppError(
        "Invalid token! It may have expired. Please request a new one"
      )
    );

  // Set new password
  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  user.passwordResetToken = undefined;
  user.passwordResetExpiresAt = undefined;
  await user.save();

  // Log user in
  createSendToken(user, 201, res);
});

exports.updatePassword = catchAsync(async (req, res, next) => {
  // Get user
  // Protect sets req.user to currently logged in user
  const user = await User.findById(req.user.id).select("+password"); // Must include

  // Check if entered password is correct
  if (!(await user.isCorrectPw(req.body.passwordCurrent, user.password)))
    return next(new AppError("Incorrect password. Please try again", 401));

  // If yes, update password
  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  await user.save();

  // Log user in
  createSendToken(user, 201, res);
});
