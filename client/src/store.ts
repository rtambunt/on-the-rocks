import { configureStore } from "@reduxjs/toolkit";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import authReducer from "./features/auth/authSlice";
import fakeDrinkReducer from "./features/drinks/fakeDrinkSlice";

const peristConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(peristConfig, authReducer);

const store = configureStore({
  reducer: {
    auth: persistedReducer,
    fakeDrinks: fakeDrinkReducer,
  },
});

export const persistor = persistStore(store);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;

export default store;
