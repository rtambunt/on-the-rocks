import { updateDrinkAPI } from "@/services/drinksService";
import { MutationError, UpdateDrinkParams, UpdateDrinkResponse } from "@/utils/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";

export function useUpdateDrink() {
  const navigate = useNavigate();

  const queryClient = useQueryClient();
  const { mutate: updateDrink } = useMutation<
    UpdateDrinkResponse,
    MutationError,
    UpdateDrinkParams
  >({
    mutationFn: (variables) =>
      updateDrinkAPI(variables.drinkId, variables.drinkData),
    onSuccess: (_, variables) => {
      toast.success("Drink Updated");
      queryClient.invalidateQueries({
        queryKey: ["userDrinks"],
        refetchType: "all",
      });
      navigate(variables.navigateTo);
    },
    onError: (err) => {
      toast.error(err.message);
      console.error(err.message);
    },
  });

  return { updateDrink };
}
