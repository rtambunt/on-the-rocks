import { useQuery } from "@tanstack/react-query";

import { fetchDrinkById } from "@/services/drinksService";

export function useGetDrink(drinkId: string | undefined) {
  const { isLoading, data: curDrink } = useQuery({
    queryKey: ["userDrink", drinkId],
    queryFn: () => fetchDrinkById(drinkId),
  });

  return { curDrink, isLoading };
}
