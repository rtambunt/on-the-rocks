import { useMutation, useQueryClient } from "@tanstack/react-query";
import toast from "react-hot-toast";

import { createDrinkAPI } from "@/services/drinksService";
import { useNavigate } from "react-router-dom";

export function useCreateDrink() {
  const navigate = useNavigate();
  const queryClient = useQueryClient();

  const { isPending, mutate: createNewDrink } = useMutation({
    mutationFn: createDrinkAPI,
    onSuccess: () => {
      toast.success("New Drink Created!");
      queryClient.invalidateQueries({ queryKey: ["userDrinks"] });
      navigate("/");
    },
    onError: (err) => toast.error(err.message),
  });

  return { isPending, createNewDrink };
}
