import { deleteDrinkAPI } from "@/services/drinksService";
import { MutationError } from "@/utils/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";

export function useDeleteDrink() {
  type DeleteMutationParams = {
    drinkId: string | number | undefined;
    navigateTo?: string;
  };
  const navigate = useNavigate();

  const queryClient = useQueryClient();
  const { mutate: deleteDrink } = useMutation<
    void,
    MutationError,
    DeleteMutationParams
  >({
    mutationFn: (variables) => deleteDrinkAPI(variables.drinkId),
    onSuccess: (_, variables) => {
      toast.success("Drink Deleted");
      queryClient.invalidateQueries({ queryKey: ["userDrinks"] });

      if (variables.navigateTo) navigate(variables.navigateTo);
    },
    onError: (err) => {
      toast.error(err.message);
      console.error(err);
    },
  });
  return { deleteDrink };
}
