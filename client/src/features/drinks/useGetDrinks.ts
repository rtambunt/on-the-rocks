import { useQuery } from "@tanstack/react-query";

import { fetchDrinks } from "@/services/drinksService";

export function useGetDrinks() {
  const { isLoading, data: userDrinks } = useQuery({
    queryKey: ["userDrinks"],
    queryFn: fetchDrinks,
    staleTime: 10000, // Cache data for 10 sec
    refetchOnMount: true,
    retry: 1, // One retry on failure
  });

  return { isLoading, userDrinks };
}
