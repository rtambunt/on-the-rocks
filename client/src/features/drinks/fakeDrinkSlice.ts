import { Drink } from "@/utils/types";
import { createSlice } from "@reduxjs/toolkit";

export type StoreState = {
  fakeDrinks: FakeDrinkState;
};

type FakeDrinkState = {
  items: Drink[];
};

const initialState = {
  items:
    localStorage.getItem !== null
      ? JSON.parse(localStorage.getItem("fakeDrinks") as string)
      : [],
};

// const initialState: Drink[] =
//   localStorage.getItem !== null
//     ? JSON.parse(localStorage.getItem("fakeDrinks") as string)
//     : [];

const fakeDrinkSlice = createSlice({
  name: "fakeDrinks",
  initialState,
  reducers: {
    setFakeDrinks(state, action) {
      state.items = action.payload;

      localStorage.setItem("fakeDrinks", JSON.stringify(state.items));
    },
    addFakeDrink(state, action) {
      const newFakeDrink = action.payload;
      state.items.push(newFakeDrink);

      //   Save to Local Storage After Adding
      localStorage.setItem("fakeDrinks", JSON.stringify(state.items));
    },
    updateFakeDrink(state, action) {
      const { drinkId, updatedData } = action.payload;

      const fakeDrinkToEdit = state.items.find(
        (fakeDrink: Drink) => fakeDrink._id === parseInt(drinkId),
      );

      if (fakeDrinkToEdit) {
        fakeDrinkToEdit.name = updatedData.name;
        fakeDrinkToEdit.description = updatedData.description;
        fakeDrinkToEdit.ingredients = updatedData.ingredients;
        fakeDrinkToEdit.instructions = updatedData.instructions;
      }

      localStorage.setItem("fakeDrinks", JSON.stringify(state.items));
    },
    deleteFakeDrink(state, action) {
      if (!action.payload) console.error("Drink ID is undefined!");

      state.items = state.items.filter(
        (fakeDrink: Drink) => fakeDrink._id !== parseInt(action.payload),
      );

      localStorage.setItem("fakeDrinks", JSON.stringify(state.items));
    },
  },
});

// --- Selectors ---
export const selectFakeDrinks = (state: StoreState) => state.fakeDrinks;
export const selectFakeDrinkById = (state: StoreState, drinkId: number) =>
  state.fakeDrinks.items.find((fakeDrink) => fakeDrink._id === drinkId);

// --- Actions + Reducer ---

export const { addFakeDrink, updateFakeDrink, deleteFakeDrink, setFakeDrinks } =
  fakeDrinkSlice.actions;
export default fakeDrinkSlice.reducer;
