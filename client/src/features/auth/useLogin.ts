import { useMutation } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { toast } from "react-hot-toast";
import { useDispatch } from "react-redux";

import { login as loginFn } from "@/services/authService";
import { loginArgs } from "@/utils/types";
import { setUser } from "./authSlice";

export function useLogin() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { mutate: login, isPending } = useMutation({
    mutationFn: ({ email, password }: loginArgs) => {
      return loginFn({ email, password });
    },
    onSuccess: (userData) => {
      dispatch(setUser(userData));

      toast.success("Logged In!");

      setTimeout(() => {
        navigate("/");
        window.location.reload();
      }, 700);
    },
    onError: (err) => {
      console.error("ERROR", err);
      toast.error("Email or password are incorrect");
    },
  });

  return { login, isPending };
}
