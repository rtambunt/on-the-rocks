import { useNavigate } from "react-router-dom";

import { useMutation, useQueryClient } from "@tanstack/react-query";
import { logout as logoutFn } from "@/services/authService";

export function useLogout() {
  const navigate = useNavigate();
  const queryClient = useQueryClient();

  const { mutate: logout, isPending } = useMutation({
    mutationFn: logoutFn,
    onSuccess: () => {
      queryClient.resetQueries({
        queryKey: ["curUser", "userDrinks", "userDrink"],
      });

      navigate(0);
    },
  });

  return { logout, isPending };
}
