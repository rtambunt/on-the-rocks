import { useForm } from "react-hook-form";
import { NavLink } from "react-router-dom";

import { Button } from "@/components/ui/Button";
import { loginArgs } from "@/utils/types";
import { useLogin } from "./useLogin";

function Login() {
  const { register, handleSubmit } = useForm<loginArgs>();
  const { login, isPending } = useLogin();

  const onSubmit = ({ email, password }: loginArgs) => {
    login({ email, password });
  };

  return (
    <div className="flex h-screen items-center justify-center">
      <div className="relative bottom-8 w-3/4 max-w-md rounded-md border bg-white px-8 pb-20 pt-12 shadow-sm sm:px-14">
        <h1 className="mb-12 text-center text-2xl">Login</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mb-14 grid grid-cols-2 grid-rows-2 items-center gap-x-2 gap-y-6">
            <label htmlFor="email">Email</label>
            <input
              className="rounded-full border px-3 py-2 text-sm"
              type="email"
              id="email"
              autoComplete="username"
              disabled={isPending}
              {...register("email")}
            />

            <label htmlFor="password">Password</label>
            <input
              className="rounded-full border px-3 py-2 text-sm"
              type="password"
              id="password"
              autoComplete="current-password"
              disabled={isPending}
              {...register("password")}
            />
          </div>

          <div className="flex justify-center">
            <Button disabled={isPending}>Submit</Button>
          </div>

          <div className="absolute bottom-5 left-0 right-0 mt-3 flex flex-col items-center justify-center gap-y-2 text-xs">
            <NavLink
              to="/forgot-password"
              className="text-blue-600 hover:underline"
            >
              Forgot Password?
            </NavLink>
            <NavLink to="/sign-up" className="text-blue-600 hover:underline">
              Don't have an account? Sign up here!
            </NavLink>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
