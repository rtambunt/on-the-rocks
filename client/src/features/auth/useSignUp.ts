import { useMutation } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import toast from "react-hot-toast";

import { signUp as signUpFn } from "@/services/authService";

function useSignUp() {
  const navigate = useNavigate();
  const { mutate: signUp, isPending } = useMutation({
    mutationFn: signUpFn,
    onSuccess: () => {
      toast.success(
        "Account has successfully been created! An email has been delivered for you to verify your user account",
      );

      navigate("/verify-email");
    },
    onError: (err) => {
      console.error("ERROR", err);
      toast.error(
        "There was an issue creating your account. Please double check that your information",
      );
    },
  });

  return { signUp, isPending };
}

export default useSignUp;
