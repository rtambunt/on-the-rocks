import { checkLogIn } from "@/services/authService";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { RootState } from "@/store";

export const fetchIsLoggedIn = createAsyncThunk<{ isLoggedIn: boolean }>(
  "user/checkLogin",
  checkLogIn,
);

const initialState = {
  user: null,
  loading: false,
  isLoggedIn: false,
  initials: null,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setUser(state, action) {
      const userData = action.payload?.data.user;
      state.user = userData;

      const firstInitial = userData.firstName[0].toUpperCase();
      const lastInitial = userData?.lastName
        ? userData.lastName[0].toUpperCase()
        : "";

      state.initials = firstInitial + lastInitial;
    },
    setInitials(state, action) {
      const firstInitial = action.payload.firstName[0].toUpperCase();
      const lastInitial = action.payload?.lastName
        ? action.payload.lastName[0].toUpperCase()
        : "";

      state.initials = firstInitial + lastInitial;
    },
    logout(state) {
      state.isLoggedIn = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchIsLoggedIn.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchIsLoggedIn.fulfilled, (state, action) => {
        state.isLoggedIn = action.payload?.isLoggedIn || false;
      })
      .addCase(fetchIsLoggedIn.rejected, (state) => {
        state.loading = false;
      });
  },
});

export const { setUser, setInitials } = authSlice.actions;
export const selectIsLoggedIn = (state: RootState) => state.auth.isLoggedIn;
export const selectInitials = (state: RootState) => state.auth.initials;

export default authSlice.reducer;
