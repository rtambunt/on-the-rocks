import { Button } from "@/components/ui/Button";
import { ErrorMessage } from "@hookform/error-message";
import { useForm } from "react-hook-form";

import useSignUp from "./useSignUp";

type SignUpInputs = {
  firstName: string;
  email: string;
  password: string;
  passwordConfirm: string;
};

function SignUp() {
  const { signUp, isPending } = useSignUp();

  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<SignUpInputs>();

  function onSubmit({
    firstName,
    email,
    password,
    passwordConfirm,
  }: SignUpInputs) {
    signUp({ firstName, email, password, passwordConfirm });
  }

  return (
    <div className="flex h-screen items-center justify-center ">
      <div className="relative bottom-8 w-3/4 max-w-md rounded-md border bg-white px-8 pb-20 pt-12 shadow-sm sm:px-14">
        <h1 className="mb-12 text-center text-2xl">Sign-up</h1>
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="text-xs sm:text-base"
        >
          <div className="mb-14 grid grid-cols-2 items-center gap-x-2 gap-y-6">
            <label htmlFor="firstName">First Name</label>

            <div className="relative">
              <input
                className="w-full rounded-full border px-3 py-2 text-sm"
                type="text"
                id="firstName"
                disabled={isPending}
                {...register("firstName", {
                  required: "First Name field is required",
                })}
              />
              <ErrorMessage
                errors={errors}
                name="firstName"
                render={({ message }) => (
                  <div className="absolute top-8 text-nowrap px-4 py-2 text-sm text-red-700">
                    {message}
                  </div>
                )}
              />
            </div>

            <label htmlFor="email">Email</label>
            <div className="relative">
              <input
                className="w-full rounded-full border px-3 py-2 text-sm"
                type="email"
                id="email"
                disabled={isPending}
                {...register("email", { required: "Email is Required" })}
              />
              <ErrorMessage
                errors={errors}
                name="email"
                render={({ message }) => (
                  <div className="absolute top-8 text-nowrap px-4 py-2 text-sm text-red-700">
                    {message}
                  </div>
                )}
              />
            </div>

            <label htmlFor="password">Password (min 8 characters)</label>
            <div className="relative">
              <input
                className="w-full rounded-full border px-3 py-2 text-sm"
                type="password"
                id="password"
                disabled={isPending}
                {...register("password", {
                  required: "Password is required",
                  minLength: {
                    value: 8,
                    message: "Must be 8 characters or more",
                  },
                })}
              />
              <ErrorMessage
                errors={errors}
                name="password"
                render={({ message }) => (
                  <div className="absolute top-8 text-nowrap px-4 py-2 text-sm text-red-700">
                    {message}
                  </div>
                )}
              />
            </div>

            <label htmlFor="passwordConfirm">Confirm Password</label>
            <div className="relative">
              <input
                className="w-full rounded-full border px-3 py-2 text-sm"
                type="password"
                id="passwordConfirm"
                disabled={isPending}
                {...register("passwordConfirm", {
                  validate: (value: string) =>
                    value === getValues().password || "Passwords must match",
                })}
              />
              <ErrorMessage
                errors={errors}
                name="passwordConfirm"
                render={({ message }) => (
                  <div className="absolute top-8 text-nowrap px-4 py-2 text-sm text-red-700">
                    {message}
                  </div>
                )}
              />
            </div>
          </div>
          <div className="text-center">
            <Button>Create Account</Button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default SignUp;
