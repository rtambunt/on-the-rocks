import toast from "react-hot-toast";

import { useMutation } from "@tanstack/react-query";
import { updatePw as updatePwFn } from "@/services/authService";
import { useNavigate } from "react-router-dom";
import { UpdatePasswordData } from "@/utils/types";

function useUpdatePw() {
  const navigate = useNavigate();

  const { mutate: updatePw, error } = useMutation({
    mutationFn: (pwData: UpdatePasswordData) => updatePwFn(pwData),
    onSuccess: () => {
      toast.success("Password Updated!");
      navigate("/");
    },
    onError: () => {
      toast.error(
        "There was a server issue while updating your password. Please try again later",
      );
      console.error(error);
    },
  });

  return { updatePw };
}

export default useUpdatePw;
