import { useMutation } from "@tanstack/react-query";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";

import { deleteMe as deleteMeFn } from "@/services/userService";
import { useLogout } from "../auth/useLogout";

function useDeleteMe() {
  const navigate = useNavigate();
  const { logout } = useLogout();

  const { mutate: deleteMe } = useMutation({
    mutationFn: deleteMeFn,
    onSuccess: () => {
      toast.success("Account Deleted");
      navigate("/");
      logout();
    },
    onError: () => {
      toast.error("Server error! Please try again later ");
    },
  });
  return { deleteMe };
}

export default useDeleteMe;
