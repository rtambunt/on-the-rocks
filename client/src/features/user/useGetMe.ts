import { getMe } from "@/services/userService";
import { useQuery } from "@tanstack/react-query";

export function useGetMe() {
  const { data: curUser } = useQuery({
    queryKey: ["curUser"],
    queryFn: getMe,
    retry: false,
  });

  return { curUser };
}
