import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useDispatch } from "react-redux";
import toast from "react-hot-toast";

import { updateUser as updateUserFn } from "@/services/userService";
import { UpdateUserData } from "@/utils/types";
import { setInitials } from "../auth/authSlice";

function useUpdateUser() {
  const queryClient = useQueryClient();
  const dispatch = useDispatch();

  const { mutate: updateUser, error } = useMutation({
    mutationFn: (userData: UpdateUserData) => updateUserFn(userData),
    onSuccess: (updatedData) => {
      toast.success("Account Updated!");
      queryClient.invalidateQueries({ queryKey: ["curUser"] });
      dispatch(
        setInitials({
          firstName: updatedData.firstName,
          lastName: updatedData?.lastName,
        }),
      );
    },
    onError: () => {
      toast.error(
        "There was a server issue while updating your account. Please try again later",
      );
      console.error(error);
    },
  });

  return { updateUser };
}

export default useUpdateUser;
