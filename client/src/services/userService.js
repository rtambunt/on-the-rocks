import axios from "axios";

export const getMe = async () => {
  try {
    const res = await axios.get(
      `${import.meta.env.VITE_API_CONSUME_URL}/api/users/me`,
      {
        withCredentials: true,
      },
    );

    return res.data.data.user;
  } catch (err) {
    console.error(
      "There was an error getting user. Please log in again!",
      err.response.data,
    );
    throw err;
  }
};

export const updateUser = async (userData) => {
  try {
    const res = await axios.patch(
      `${import.meta.env.VITE_API_CONSUME_URL}/api/users/update-me`,
      userData,
      {
        withCredentials: true,
      },
    );
    return res.data.data.user;
  } catch (err) {
    console.error("Could not update user");
    throw err;
  }
};

export const deleteMe = async () => {
  try {
    const res = await axios.delete(
      `${import.meta.env.VITE_API_CONSUME_URL}/api/users/delete-me`,
      {
        withCredentials: true,
      },
    );
    return res;
  } catch (err) {
    console.error("Could not delete user");
    throw err;
  }
};
