import axios from "axios";

// - Urls -
const userDrinksUrl = `${import.meta.env.VITE_API_CONSUME_URL}/api/users/me/drinks`;

export const fetchDrinks = async () => {
  try {
    const res = await axios.get(userDrinksUrl, { withCredentials: true });
    return res.data.data.drinks;
  } catch (err) {
    console.error("Error fetching drinks", err.response.data);
    throw new Error("There was an error getting user drinks!");
  }
};

export const fetchDrinkById = async (drinkId) => {
  const userDrinkUrl = `${userDrinksUrl}/${drinkId}`;
  try {
    const res = await axios.get(userDrinkUrl, { withCredentials: true });
    return res.data.data.drink;
  } catch (err) {
    console.error(
      `Error fetching drink with id: ${drinkId}`,
      err.response.data,
    );
    throw new Error(
      `There was an error getting user drink with id: ${drinkId}!`,
    );
  }
};

export const createDrinkAPI = async (drinkData) => {
  try {
    const res = await axios.post(userDrinksUrl, drinkData, {
      withCredentials: true,
    });
    return res.data;
  } catch (err) {
    console.error(`Error creating new drink!`, err.response.data);
    throw err;
  }
};

export const updateDrinkAPI = async (drinkId, drinkData) => {
  const userDrinkUrl = `${userDrinksUrl}/${drinkId}`;

  try {
    const res = await axios.patch(userDrinkUrl, drinkData, {
      withCredentials: true,
    });
  
    return res.data;
  } catch (err) {
    console.error(
      `Error updating drink with id: ${drinkId}`,
      err.response.data,
    );
    throw err;
  }
};

export const deleteDrinkAPI = async (drinkId) => {
  const userDrinkUrl = `${userDrinksUrl}/${drinkId}`;
  try {
    await axios.delete(userDrinkUrl, { withCredentials: true });
  } catch (err) {
    console.error(
      `Error deleting drink with id: ${drinkId}`,
      err.response.data,
    );
    throw err;
  }
};
