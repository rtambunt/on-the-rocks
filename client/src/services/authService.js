import axios from "axios";

export const signUp = async (newUser) => {
  const signUpUrl = `${import.meta.env.VITE_API_CONSUME_URL}/api/users/sign-up`;
  try {
    const res = await axios.post(signUpUrl, newUser, {
      withCredentials: true,
    });
    return res.data;
  } catch (err) {
    console.error("There was an error with sign up!", err.response.data);
    throw err;
  }
};

export const login = async (loginInfo) => {
  // email + password is req'd for loginInfo

  const loginUrl = `${import.meta.env.VITE_API_CONSUME_URL}/api/users/login`;

  try {
    const res = await axios.post(loginUrl, loginInfo, {
      withCredentials: true,
    });
    return res.data;
  } catch (err) {
    console.error("There was an error with login!", err.response.data);
    throw err;
  }
};

export const logout = async () => {
  try {
    await axios.get(
      `${import.meta.env.VITE_API_CONSUME_URL}/api/users/logout`,
      { withCredentials: true },
    );
  } catch (err) {
    console.error("There was an error with login!", err.response.data);
    throw err;
  }
};



export const checkLogIn = async () => {
  try {
    const res = await axios.get(
      `${import.meta.env.VITE_API_CONSUME_URL}/api/users/is-logged-in`,
      { withCredentials: true },
    );

    return res.data;
  } catch (err) {
    console.error("Could not access api route", err.response.data);
    throw err;
  }
};

export const updatePw = async (pwData) => {
  try {
    const res = await axios.patch(
      `${import.meta.env.VITE_API_CONSUME_URL}/api/users/update-password`,
      pwData,
      {
        withCredentials: true,
      },
    );
    return res;
  } catch (err) {
    console.error("Could not update password");
    throw err;
  }
};
