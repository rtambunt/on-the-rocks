import { Ingredient, Instruction } from "../utils/types";

type FakeDrink = {
  _id: number;
  name: string;
  description: string;
  ingredients: Ingredient[];
  instructions: Instruction[];
};

type FakeDrinkList = FakeDrink[];

export const fakeDrinkList: FakeDrinkList = [
  {
    _id: 1,
    name: "Margarita",
    description: "A classic tequila drink",
    ingredients: [
      {
        name: "Tequila",
        type: "Alcohol",
        quantity: "1.5",
        units: "oz",
      },
      {
        name: "Lime Juice",
        type: "Juice",
        quantity: "1",
        units: "oz",
      },
    ],
    instructions: [
      {
        instruction:
          "Rub the rim of the glass with the lime slice to make the salt stick to it.",
        orderNum: 1,
      },
    ],
  },
  {
    _id: 2,
    name: "Sidecar",
    description: "For cognac lovers",
    ingredients: [
      {
        name: "Cognac",
        type: "Alcohol",
        quantity: "1.5",
        units: "oz",
      },
      {
        name: "Triple Sec",
        type: "Liqueur",
        quantity: "0.75",
        units: "oz",
      },
    ],
    instructions: [
      {
        instruction: "Coat the glass rim with sugar.",
        orderNum: 1,
      },
      {
        instruction:
          "Add cognac, triple sec, lemon juice, and ice to a shaker and shake until cold",
        orderNum: 2,
      },
    ],
  },
];
