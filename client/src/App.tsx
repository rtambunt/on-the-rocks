import { useEffect } from "react";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { Toaster } from "react-hot-toast";

import AppLayout from "./pages/AppLayout";
import NotFound from "./pages/NotFound";
import Home from "./pages/Home";
import SignIn from "./features/auth/Login";
import SignUp from "./features/auth/SignUp";
import DrinkDetail from "./pages/DrinkDetail/DrinkDetail";
import CreateDrink from "./pages/CreateDrink";
import EditDrinkForm from "./pages/EditDrinkForm/EditDrinkForm";
import { useDispatch } from "react-redux";
import { fetchIsLoggedIn } from "./features/auth/authSlice";
import { AppDispatch } from "./store";
import Profile from "./pages/Profile";
import VerifyEmail from "./pages/Account/VerifyEmail";
import ResetPassword from "./pages/Account/ResetPassword";
import ForgotPassword from "./pages/Account/ForgotPassword";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 0,
    },
  },
});

function App() {
  const router = createBrowserRouter([
    {
      // - Display header with page -
      element: <AppLayout />,
      errorElement: <NotFound />,
      children: [
        { path: "/", element: <Home /> },
        { path: "/drink/:drinkId", element: <DrinkDetail /> },
        { path: "/edit-drink/:drinkId", element: <EditDrinkForm /> },
        {
          element: <CreateDrink />,
          path: "/create-drink",
        },
        { path: "/profile", element: <Profile /> },
      ],
    },
    {
      element: <SignIn />,
      path: "/sign-in",
      errorElement: <NotFound />,
    },
    {
      element: <SignUp />,
      path: "/sign-up",
      errorElement: <NotFound />,
    },
    {
      element: <VerifyEmail />,
      path: "/verify-email/:token?",
      errorElement: <NotFound />,
    },
    {
      element: <ResetPassword />,
      path: "/reset-password/:token",
      errorElement: <NotFound />,
    },
    {
      element: <ForgotPassword />,
      path: "/forgot-password",
      errorElement: <NotFound />,
    },
  ]);

  // --- Fetch login status ---
  const dispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    dispatch(fetchIsLoggedIn());
  }, [dispatch]);

  return (
    <>
      <QueryClientProvider client={queryClient}>
        <RouterProvider router={router} />
        <Toaster
          position="top-center"
          gutter={12}
          containerStyle={{ margin: "8px" }}
          toastOptions={{
            success: { duration: 3000 },
            error: { duration: 5000 },
            style: {
              fontSize: "16px",
              maxWidth: "500px",
              padding: "16px 24px",
              backgroundColor: "var(--color-grey-0)",
              color: "var(--color-grey-700)",
            },
          }}
        />
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </>
  );
}

export default App;
