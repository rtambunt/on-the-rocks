// --- Form Styles ---
const labelStyles = "font-semibold md:text-lg";
const inputStyles = "w-full rounded-md border px-3 py-2 text-sm";
const inputDivStyles = "flex w-full flex-col gap-y-2";

export { labelStyles, inputStyles, inputDivStyles };
