// --- Drinks ---
export type Ingredient = {
  _id?: number | null;
  name: string;
  type: string;
  quantity: string;
  units: string;
};

export type Instruction = {
  id?: number | null;
  orderNum: number | null;
  instruction: string;
};

export type Drink = {
  _id: number;
  name: string;
  description: string;
  created_at?: string;
  image?: string;
  ingredients: Ingredient[];
  instructions: Instruction[];
};

// User + Auth

export type User = {
  _id: string;
  firstName: string;
  lastName?: string;
  email: string;
  drinks: Drink[];
};

export type loginArgs = {
  email: string;
  password: string;
};

export type UpdatePasswordData = {
  passwordCurrent: string;
  password: string;
  passwordConfirm: string;
};

export type UpdateUserData = {
  firstName?: string;
  lastName?: string;
  email?: string;
};

// --- Form Types ---

export type DrinkFormInputs = {
  id: number;
  drinkImage: string;
  drinkName: string;
  drinkDescription: string;
  drinkIngredients: Ingredient[];
  drinkInstructions: Instruction[];
};

// --- Mutation Types ---

export type PatchDrinkData = Partial<Drink>;

export type UpdateDrinkParams = {
  drinkId: string | undefined;
  drinkData: PatchDrinkData | FormData;
  navigateTo: string;
};

export type MutationError = {
  message: string;
};

// --- Response Types

export type ResponseData = { drink: Drink };

export type UpdateDrinkResponse = {
  status: string;
  data: ResponseData;
};
