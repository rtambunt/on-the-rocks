import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";
import { Drink } from "./types";

// -- Shadcn Util --
export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

// -- Form Valid Object Converting --
export function convertDrinkToFormFormat(drink: Drink) {
  if (!drink) return {};
  return {
    id: drink._id,
    drinkName: drink.name,
    drinkDescription: drink.description,
    drinkIngredients: drink.ingredients,
    drinkInstructions: drink.instructions,
  };
}

// -- Getting User Profile Initials --
export function getUserInitials(
  firstName: string,
  lastName: string | undefined,
) {
  if (!lastName) return firstName[0].toUpperCase();

  return (firstName[0] + lastName[0]).toUpperCase();
}
