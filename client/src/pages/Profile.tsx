import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";

import { Button } from "@/components/ui/Button";
import { useGetMe } from "@/features/user/useGetMe";
import useUpdatePw from "@/features/user/useUpdatePw";
import { inputStyles } from "@/utils/tailwindStyles";
import { UpdatePasswordData } from "@/utils/types";
import EditUserForm from "@/components/user/EditUserForm";
import useDeleteMe from "@/features/user/useDeleteMe";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogTitle,
  DialogTrigger,
  DialogHeader,
} from "@/components/ui/Dialog";

function Profile() {
  const [userEditTrue, setUserEditTrue] = useState(false);

  const { curUser } = useGetMe();
  const { updatePw } = useUpdatePw();
  const { deleteMe } = useDeleteMe();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UpdatePasswordData>();

  const handleUpdatePwSubmit: SubmitHandler<UpdatePasswordData> = (pwData) => {
    updatePw(pwData);
  };

  // -- Tailwind Styles --
  const errorStyles =
    "absolute -right-80 top-0 text-nowrap text-sm text-red-700";

  return (
    <div className="mx-auto mb-20 mt-10 max-w-[40rem]">
      <h1 className="mb-10 text-center text-3xl font-semibold">Profile</h1>

      <div className="flex flex-col gap-y-4">
        {!userEditTrue ? (
          /* - Account Info - */
          <div className="rounded-lg bg-gray-100 px-16 py-12">
            <div className="mb-5 flex items-center justify-between">
              <h2 className="text-2xl font-semibold ">Account Info</h2>
              <Button onClick={() => setUserEditTrue(true)}>
                Edit Profile
              </Button>
            </div>

            <div className="ml-8 grid grid-cols-2 gap-y-1">
              <p className="font-semibold">First Name: </p>
              <p>{curUser?.firstName}</p>

              <p className="font-semibold">Last Name: </p>
              <p>{curUser?.lastName}</p>

              <p className="font-semibold">Email: </p>
              <p>{curUser?.email}</p>
            </div>
          </div>
        ) : (
          <EditUserForm curUser={curUser} setUserEditTrue={setUserEditTrue} />
        )}

        {/* - Update Password - */}
        <div className="rounded-lg bg-gray-100 px-16 py-10">
          <h2 className="mb-5 text-2xl font-semibold">Update Password</h2>
          <form onSubmit={handleSubmit(handleUpdatePwSubmit)} className="ml-8 ">
            <div className="grid grid-cols-2 gap-y-1">
              <div className="flex items-center">
                <label htmlFor="curPassword" className="font-semibold">
                  Current Password:
                </label>
              </div>

              <div className="relative">
                <input
                  id="curPassword"
                  type="password"
                  className={inputStyles}
                  {...register("passwordCurrent", {
                    required: "Your Current Password is Required",
                  })}
                />
                <ErrorMessage
                  errors={errors}
                  name="passwordCurrent"
                  render={({ message }) => (
                    <div className={errorStyles}>{message}</div>
                  )}
                />
              </div>

              <div className="flex items-center">
                <label htmlFor="newPassword" className="font-semibold">
                  New Password:
                </label>
              </div>

              <div className="relative">
                <input
                  id="newPassword"
                  type="password"
                  className={inputStyles}
                  {...register("password", {
                    required: "New Password is Required",
                  })}
                />
                <ErrorMessage
                  errors={errors}
                  name="password"
                  render={({ message }) => (
                    <div className={errorStyles}>{message}</div>
                  )}
                />
              </div>

              <div className="flex items-center">
                <label htmlFor="newPasswordConfirm" className="font-semibold">
                  Confirm New Password:
                </label>
              </div>
              <div className="relative">
                <input
                  id="newPasswordConfirm"
                  type="password"
                  className={inputStyles}
                  {...register("passwordConfirm", {
                    required: "Password Confirmation is Required",
                  })}
                />
                <ErrorMessage
                  errors={errors}
                  name="passwordConfirm"
                  render={({ message }) => (
                    <div className={errorStyles}>{message}</div>
                  )}
                />
              </div>
            </div>

            <Button className="float-right mr-5 mt-7">Update</Button>
          </form>
        </div>
      </div>

      {/* - Delete Option - */}

      <Dialog>
        <DialogTrigger className="mt-10 flex w-full justify-end">
          <Button className="mr-2" variant="destructive">
            Delete Account
          </Button>
        </DialogTrigger>
        <DialogContent>
          <DialogHeader>
            <DialogTitle>Are you absolutely sure?</DialogTitle>
            <DialogDescription>
              <div>
                This action cannot be undone. This will{" "}
                <span className="font-bold">permanently delete</span> your
                account and remove your data from our servers
              </div>
            </DialogDescription>
          </DialogHeader>
          <div className="flex justify-end gap-x-2">
            <Button
              onClick={() => deleteMe()}
              type="button"
              variant="destructive"
            >
              Yes, delete this drink
            </Button>
            <DialogClose>
              <Button>Close</Button>
            </DialogClose>
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
}

export default Profile;
