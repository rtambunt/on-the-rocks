import { useEffect, useState } from "react";
import { FaRegEdit } from "react-icons/fa";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import DrinkDisplay from "../components/drinks/DrinkDisplay";
import { useGetDrinks } from "@/features/drinks/useGetDrinks";
import { Drink } from "@/utils/types";
import { Button } from "@/components/ui/Button";
import { selectIsLoggedIn } from "@/features/auth/authSlice";
import { fakeDrinkList } from "@/services/fakeApi";
import {
  selectFakeDrinks,
  setFakeDrinks,
} from "@/features/drinks/fakeDrinkSlice";

function Home() {
  const [editMode, setEditMode] = useState(false);
  const [localDrinks, setLocalDrinks] = useState<Drink[]>([]);

  const navigate = useNavigate();

  const dispatch = useDispatch();
  const isLoggedIn = useSelector(selectIsLoggedIn);

  const { isLoading, userDrinks } = useGetDrinks();
  const reduxDrinks = useSelector(selectFakeDrinks);

  useEffect(() => {
    const localStorageDrinks = localStorage.getItem("fakeDrinks");

    if (localStorageDrinks === null) {
      dispatch(setFakeDrinks(fakeDrinkList));
      setLocalDrinks(fakeDrinkList);
    } else {
      setLocalDrinks(JSON.parse(localStorageDrinks));
    }
  }, [dispatch, reduxDrinks]);

  return (
    <div className="flex flex-col gap-y-10 bg-background px-8 pt-14 text-amber-950">
      <h1 className="text-4xl font-semibold">Hi! What can I get you today?</h1>
      {!isLoggedIn && (
        <p className="text-bold font-semibold">
          (Local Storage Version! Please Log In to Persist Drinks Across all
          Devices)
        </p>
      )}
      <div>
        <div className="mb-5 flex items-center justify-between lg:mb-6">
          <div className="flex items-center gap-x-4">
            <h2 className=" text-2xl font-semibold">All Drinks</h2>
            <button onClick={() => setEditMode(!editMode)}>
              <FaRegEdit className="text-slate-600" size={22} />
            </button>
          </div>
          <Button className="sm:hidden">
            <NavLink to="/create-drink">+</NavLink>
          </Button>
          <Button
            onClick={() => navigate("/create-drink")}
            className="hidden sm:block"
          >
            Add Drink (+)
          </Button>
        </div>

        <div className="grid gap-4 pb-16 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5">
          {/* User drinks */}
          {!isLoggedIn ? (
            localDrinks.map((drink) => (
              <DrinkDisplay
                drink={drink}
                editMode={editMode}
                key={`${drink._id}-${drink.name}`}
              />
            ))
          ) : isLoading ? (
            <div>Is Loading... </div>
          ) : (
            userDrinks?.map((drink: Drink) => (
              <DrinkDisplay drink={drink} editMode={editMode} key={drink._id} />
            ))
          )}
        </div>
      </div>
    </div>
  );
}

export default Home;
