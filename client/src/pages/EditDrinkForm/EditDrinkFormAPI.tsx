import { useForm, SubmitHandler, useFieldArray } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";
import { useNavigate, useParams } from "react-router-dom";
import { IoMdArrowRoundBack } from "react-icons/io";
import { useEffect } from "react";

import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogTitle,
  DialogDescription,
  DialogClose,
  DialogHeader,
} from "@/components/ui/Dialog";
import { Button } from "../../components/ui/Button";
import {
  inputDivStyles,
  inputStyles,
  labelStyles,
} from "@/utils/tailwindStyles";
import { DrinkFormInputs } from "@/utils/types";
import { convertDrinkToFormFormat } from "@/utils/utils";

import { useGetDrink } from "@/features/drinks/useGetDrink";
import { useUpdateDrink } from "@/features/drinks/useUpdateDrink";
import { useDeleteDrink } from "@/features/drinks/useDeleteDrink";

function EditDrinkFormAPI() {
  const navigate = useNavigate();

  // --- React Query ---
  const { drinkId } = useParams();

  const { isLoading, curDrink } = useGetDrink(drinkId);

  const { updateDrink } = useUpdateDrink();
  const { deleteDrink } = useDeleteDrink();

  // --- React Hook Form ---
  const editValues = convertDrinkToFormFormat(curDrink);

  const {
    register,
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm<DrinkFormInputs>({ defaultValues: editValues });

  const {
    fields: ingredientFields,
    append: ingredientAppend,
    remove: ingredientRemove,
  } = useFieldArray({ control, name: "drinkIngredients" });

  const {
    fields: instructionFields,
    append: instructionAppend,
    remove: instructionRemove,
  } = useFieldArray({ control, name: "drinkInstructions" });

  const onSubmit: SubmitHandler<DrinkFormInputs> = (drinkData) => {
    // Check that no empty ingredients or instructions were added
    const validIngredients = drinkData.drinkIngredients.filter((ingredient) => {
      return ingredient.name || ingredient.quantity || ingredient.units;
    });

    const validInstructions = drinkData.drinkInstructions.filter(
      (instruction) => {
        return instruction.orderNum || instruction.instruction;
      },
    );

    const formData = new FormData();
    formData.append("name", drinkData.drinkName);
    formData.append("image", drinkData.drinkImage[0]);
    formData.append("description", drinkData.drinkDescription);
    formData.append("ingredients", JSON.stringify(validIngredients));
    formData.append("instructions", JSON.stringify(validInstructions));

    updateDrink({
      drinkId,
      drinkData: formData,
      navigateTo: `/drink/${drinkId}`,
    });
  };

  function handleDelete() {
    deleteDrink({ drinkId, navigateTo: `/` });
  }

  useEffect(() => {
    if (curDrink) {
      const editValues = convertDrinkToFormFormat(curDrink);
      reset(editValues);
    }
  }, [curDrink, reset]);

  if (isLoading) return <p>Loading...</p>;
  if (!curDrink) return <p>Drink not found</p>;

  return (
    <div className="mx-auto mb-14 mt-10  max-w-[35rem]  px-8">
      <div>
        <div className="flex items-center justify-between">
          <button
            onClick={() => navigate(-1)}
            className="rounded-full border-2 px-3 py-2"
          >
            <IoMdArrowRoundBack size={25} />
          </button>
        </div>
        <div>
          <h1 className="text-center text-xl font-semibold md:text-3xl">
            Edit Drink: {curDrink.name}
          </h1>

          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col items-start justify-center gap-y-8"
          >
            <input
              type="hidden"
              {...register("id")} // Hidden input for drink id
            />
            <div className={inputDivStyles}>
              <label className={labelStyles} htmlFor="drinkName">
                Name
              </label>
              <input
                id="drinkName"
                type="text"
                className={inputStyles}
                {...register("drinkName", {
                  required: "Drink Name is Required",
                })}
              />
              <ErrorMessage
                errors={errors}
                name="drinkName"
                render={({ message }) => (
                  <div className="absolute top-8 text-nowrap px-4 py-2 text-sm text-red-700">
                    {message}
                  </div>
                )}
              />
            </div>

            <div className={inputDivStyles}>
              <label className={labelStyles} htmlFor="drinkImage">
                New Image
              </label>
              <input
                id="drinkImage"
                type="file"
                accept="image/*"
                {...register("drinkImage")}
              />
            </div>

            <div className={inputDivStyles}>
              <label className={labelStyles} htmlFor="drinkDescription">
                Description
              </label>
              <textarea
                id="drinkDescription"
                className={inputStyles}
                {...register("drinkDescription")}
              />
            </div>

            {/* --- Edit Ingredients --- */}
            <div className="w-full">
              <label className={labelStyles}>Ingredients</label>
              <div className="mt-4 flex flex-col gap-y-1">
                <div className="mb-1 flex gap-3 text-sm font-semibold">
                  <h3 className="w-1/4">Name</h3>
                  <h3 className="w-1/4">Type</h3>
                  <h3 className="w-1/4">Quantity</h3>
                  <h3 className="w-1/4">Units</h3>
                  <h3 className="w-10"></h3>
                </div>
                {ingredientFields.map((field, index) => (
                  <div key={field.id} className="flex gap-2">
                    <input
                      type="hidden"
                      {...register(`drinkIngredients.${index}._id`)} // Hidden input for ingredient ID
                    />
                    <input
                      type="text"
                      className={inputStyles}
                      {...register(`drinkIngredients.${index}.name`)}
                      placeholder="Ingredient Name"
                    />

                    <input
                      type="text"
                      className={inputStyles}
                      {...register(`drinkIngredients.${index}.type`)}
                      placeholder="Type"
                    />
                    <input
                      type="text"
                      className={inputStyles}
                      {...register(`drinkIngredients.${index}.quantity`)}
                      placeholder="Quantity"
                    />
                    <input
                      type="text"
                      className={inputStyles}
                      {...register(`drinkIngredients.${index}.units`)}
                      placeholder="Units"
                    />
                    <Button
                      type="button"
                      variant="destructive"
                      onClick={() => {
                        ingredientRemove(index);
                      }}
                    >
                      -
                    </Button>
                  </div>
                ))}
                <Button
                  type="button"
                  className="mt-3"
                  onClick={() =>
                    ingredientAppend({
                      _id: null,
                      name: "",
                      type: "",
                      quantity: "",
                      units: "",
                    })
                  }
                >
                  Add Ingredient (+)
                </Button>
              </div>
            </div>

            {/* --- Edit Drink Instructions */}
            <div className="w-full">
              <label className={labelStyles} htmlFor="drinkInstructions">
                Instructions
              </label>
              <div className="mt-4 flex flex-col">
                <div className="mb-1 flex gap-3 text-sm font-semibold">
                  <h3 className="w-8 px-3">#</h3>
                  <h3 className="pl-1">Instruction</h3>
                </div>
                {instructionFields.map((field, index) => (
                  <div className="flex items-center gap-2" key={field.id}>
                    <input
                      type="hidden"
                      {...register(`drinkInstructions.${index}.id`)} // Hidden input for ingredient ID
                    />
                    <input
                      type="text"
                      className="w-8 rounded-md border p-3 text-center text-sm"
                      {...register(`drinkInstructions.${index}.orderNum`)}
                      placeholder="Order #"
                    />
                    <textarea
                      id="drinkInstructions"
                      // className={inputStyles}
                      className="w-full rounded-md border px-3 pt-3 text-sm"
                      {...register(`drinkInstructions.${index}.instruction`)}
                    />
                    <Button
                      type="button"
                      variant="destructive"
                      onClick={() => {
                        instructionRemove(index);
                      }}
                    >
                      -
                    </Button>
                  </div>
                ))}
                <Button
                  type="button"
                  className="mt-3"
                  onClick={() => {
                    instructionAppend({
                      id: null,
                      orderNum: null,
                      instruction: "",
                    });
                  }}
                >
                  Add Instruction (+)
                </Button>
              </div>
            </div>

            <div className="mt-3 flex w-full justify-end gap-x-1">
              <Dialog>
                <DialogTrigger>
                  <Button
                    type="button"
                    variant="destructive"
                    className="w-full"
                  >
                    Delete Drink
                  </Button>
                </DialogTrigger>
                <DialogContent>
                  <DialogHeader>
                    <DialogTitle>Are you absolutely sure?</DialogTitle>
                    <DialogDescription>
                      <div>
                        This action cannot be undone. This will
                        <span className="font-bold">permanently delete</span>
                        your drink and remove your data from our servers.
                      </div>
                    </DialogDescription>
                  </DialogHeader>
                  <div className="flex justify-end gap-x-2">
                    <Button
                      onClick={() => handleDelete()}
                      type="button"
                      variant="destructive"
                    >
                      Yes, delete this drink
                    </Button>
                    <DialogClose>
                      <Button>Close</Button>
                    </DialogClose>
                  </div>
                </DialogContent>
              </Dialog>
              <Button>Save Changes</Button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
export default EditDrinkFormAPI;
