import { selectIsLoggedIn } from "@/features/auth/authSlice";
import { useSelector } from "react-redux";
import EditDrinkFormAPI from "./EditDrinkFormAPI";
import EditDrinkFormFakeAPI from "./EditDrinkFormFakeAPI";

function EditDrinkForm() {
  const isLoggedIn = useSelector(selectIsLoggedIn);
  if (isLoggedIn) return <EditDrinkFormAPI />;
  return <EditDrinkFormFakeAPI />;
}

export default EditDrinkForm;
