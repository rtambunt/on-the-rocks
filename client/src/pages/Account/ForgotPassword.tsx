import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import axios from "axios";

import { Button } from "@/components/ui/Button";
import { inputStyles, labelStyles } from "@/utils/tailwindStyles";

function ForgotPassword() {
  type ForgotPwInput = {
    email: string;
  };

  const [status, setStatus] = useState("");

  const navigate = useNavigate();
  const { register, handleSubmit } = useForm<ForgotPwInput>();

  const submitForgotPw: SubmitHandler<ForgotPwInput> = async (forgotPwData) => {
    try {
      await axios.post(
        `${import.meta.env.VITE_API_CONSUME_URL}/api/users/forgot-password`,
        forgotPwData,
      );

      setStatus("Password reset email sent!");

      setTimeout(() => {
        navigate("/sign-in");
      }, 3000);
    } catch (err) {
      console.error(err);
      setStatus(
        "Server error. Unable to send password reset email. Please try again later",
      );
    }
  };

  return (
    <div className="mx-auto mt-16 w-5/6 md:w-1/2">
      <h1 className="mb-10 text-center text-2xl font-semibold md:text-3xl">
        Forgot Password
      </h1>
      <div className="rounded-lg border px-10 py-16">
        <form
          onSubmit={handleSubmit(submitForgotPw)}
          className="flex flex-col gap-y-8"
        >
          <div className="flex flex-col gap-y-5">
            <label className={labelStyles} htmlFor="email">
              Email Address
            </label>
            <input
              id="email"
              className={inputStyles}
              type="email"
              {...register("email")}
            />
          </div>

          <div className="flex justify-end">
            <Button className="mr-4">Send Reset Password Email</Button>
          </div>
          <p className="text-lg">
            <span className="font-semibold">Status:</span> {status}
          </p>
        </form>
      </div>
    </div>
  );
}

export default ForgotPassword;
