import { Button } from "@/components/ui/Button";
import { inputStyles } from "@/utils/tailwindStyles";
import axios from "axios";
import { useEffect, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";

function VerifyEmail() {
  type ResendVerifyInput = {
    email: string;
  };

  const { token } = useParams();
  const navigate = useNavigate();

  const [status, setStatus] = useState(
    "Please check your email inbox for verification link",
  );

  const { register, handleSubmit } = useForm<ResendVerifyInput>();

  const submitResendEmailVerify: SubmitHandler<ResendVerifyInput> = async (
    resendData,
  ) => {
    try {
      await axios.post(
        `${import.meta.env.VITE_API_CONSUME_URL}/api/users/resend-email-verification`,
        resendData,
      );
      setStatus("New verification email sent! Please check your inbox");
    } catch (err) {
      console.error(err);
      setStatus("Unable to resend email verification. Please try again later");
    }
  };

  useEffect(() => {
    const verifyEmail = async () => {
      try {
        setStatus("Verifying...");
        await axios.post(
          `${import.meta.env.VITE_API_CONSUME_URL}/api/users/verify-email/${token}`,
        );

        setStatus("Verified! You may now sign in to your account");

        setTimeout(() => {
          navigate("/sign-in");
        }, 3000);
      } catch (err) {
        console.error(err);
        setStatus(
          "Unable to verify account. Your token may be expired. Please try again.",
        );
      }
    };
    if (token) verifyEmail();
  }, [navigate, token]);

  return (
    <div className="mx-auto mt-16 w-1/2">
      <h1 className="mb-10 text-center text-2xl font-semibold md:text-3xl">
        Email Verification
      </h1>
      <div className="rounded-lg border px-10 py-16">
        <p className="mb-5 text-lg">
          <span className="font-semibold">Status:</span> {status}
        </p>
        <div>
          <hr />
          <h2 className="mb-5 mt-8 text-lg font-semibold">
            Resend Email Verification
          </h2>
          <form onSubmit={handleSubmit(submitResendEmailVerify)}>
            <div className="flex flex-col gap-y-2">
              <label className="font-semibold" htmlFor="email">
                Email Address
              </label>
              <input
                id="email"
                className={inputStyles}
                type="email"
                {...register("email")}
              />
              <div>
                <Button className="ml-2">Resend Email Verification</Button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default VerifyEmail;
