import axios from "axios";
import { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";

import { Button } from "@/components/ui/Button";
import { inputStyles, labelStyles } from "@/utils/tailwindStyles";

function ResetPassword() {
  type PwInputData = {
    password: string;
    passwordConfirm: string;
  };

  const [status, setStatus] = useState("");

  const { token } = useParams();
  const navigate = useNavigate();
  const { register, handleSubmit } = useForm<PwInputData>();

  const submitResetPw: SubmitHandler<PwInputData> = async (pwData) => {
    try {
      await axios.patch(
        `${import.meta.env.VITE_API_CONSUME_URL}/api/users/reset-password/${token}`,
        pwData,
      );

      setStatus("Your password has been updated!");

      setTimeout(() => {
        navigate("/sign-in");
      }, 3000);
    } catch (err) {
      console.error(err);
      setStatus(
        "Unable to change password. Your reset token may be expired. Please try again",
      );
    }
  };

  return (
    <div className="mx-auto mt-16 w-5/6 md:w-1/2">
      <h1 className="mb-10 text-center text-2xl font-semibold md:text-3xl">
        Reset Password
      </h1>
      <div className="rounded-lg border px-10 py-16">
        <form
          onSubmit={handleSubmit(submitResetPw)}
          className="mb-10 flex flex-col gap-y-5"
        >
          <label className={labelStyles} htmlFor="password">
            New Password
          </label>
          <input
            id="password"
            className={inputStyles}
            type="password"
            {...register("password")}
          />
          <label className={labelStyles} htmlFor="passwordConfirm">
            Confirm New Password
          </label>
          <input
            id="passwordConfirm"
            className={inputStyles}
            type="password"
            {...register("passwordConfirm")}
          />
          <div className="flex justify-end">
            <Button className="mr-4">Update Password</Button>
          </div>
        </form>
        <p className="text-lg">
          <span className="font-semibold">Status:</span> {status}
        </p>
      </div>
    </div>
  );
}

export default ResetPassword;
