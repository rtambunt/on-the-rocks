import DrinkDetailAPI from "@/pages/DrinkDetail/DrinkDetailAPI";
import DrinkDetailFakeAPI from "@/pages/DrinkDetail/DrinkDetailFakeAPI";
import { selectIsLoggedIn } from "@/features/auth/authSlice";
import { useSelector } from "react-redux";

function DrinkDetail() {
  const isLoggedIn = useSelector(selectIsLoggedIn);
  if (isLoggedIn) return <DrinkDetailAPI />;

  return <DrinkDetailFakeAPI />;
}

export default DrinkDetail;
