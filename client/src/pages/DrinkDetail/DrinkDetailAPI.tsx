import { useParams, useNavigate, Link } from "react-router-dom";
import { IoMdArrowRoundBack, IoMdArrowRoundForward } from "react-icons/io";
import { FaRegEdit } from "react-icons/fa";

import { Ingredient, Instruction } from "@/utils/types";
import { useGetDrink } from "@/features/drinks/useGetDrink";
import { BiDrink } from "react-icons/bi";

function DrinkDetailAPI() {
  const navigate = useNavigate();

  // --- React Query ---
  const { drinkId } = useParams();

  const { curDrink, isLoading } = useGetDrink(drinkId);

  // --- Tailwind Styles ---
  const heading2 = "mt-2 text-lg font-semibold";

  if (isLoading) return <p>Loading...</p>;

  return (
    <div className="mx-auto mb-6 mt-10 w-5/6 max-w-[45rem] sm:w-4/5 md:w-3/4">
      <div>
        <div className="flex items-center justify-between">
          <button
            onClick={() => navigate("/")}
            className="rounded-full border-2 px-3 py-2"
          >
            <IoMdArrowRoundBack size={25} />
          </button>
          <Link to={`/edit-drink/${drinkId}`}>
            <FaRegEdit size={25} />
          </Link>
        </div>

        <div>
          <h1 className="mt-6 text-center text-3xl font-semibold">
            {curDrink?.name}
          </h1>
          {curDrink?.image ? (
            <img
              src={curDrink?.image}
              alt={curDrink?.name + "Photo"}
              className="mx-auto mt-10 w-56 rounded-md border"
            />
          ) : (
            <div className="mx-auto mt-10 flex h-1/4 w-1/4 items-center justify-center rounded-lg border py-5">
              <BiDrink className="h-full w-full" />
            </div>
          )}

          <div className="mt-6 flex flex-col gap-y-6">
            <div>
              <h2 className={heading2}>Description</h2>
              <hr />
              <p className="mt-3">{curDrink.description}</p>
            </div>

            <div className="border-4 border-primary bg-background_dark px-8 pb-6 pt-3">
              <h2 className={heading2}>Ingredients</h2>
              <hr />
              <ul className=" list-inside list-disc pl-4">
                {curDrink?.ingredients?.map((ingredient: Ingredient) => (
                  <li key={ingredient.name}>{ingredient.name}</li>
                ))}
              </ul>
            </div>

            <div>
              <h2 className={heading2}>Quantities</h2>
              <hr />

              <ul className="mt-3 list-inside list-disc pl-4">
                {curDrink?.ingredients?.map((ingredient: Ingredient) => (
                  <li
                    key={
                      ingredient.name +
                      "-" +
                      ingredient.quantity +
                      ingredient.units
                    }
                  >
                    {ingredient.name}: {ingredient.quantity} {ingredient.units}
                  </li>
                ))}
              </ul>
            </div>

            <div>
              <h2 className={heading2}>Instructions</h2>
              <hr />
              <ol className="mt-3 list-inside list-decimal pl-4">
                {curDrink?.instructions?.map((instruction: Instruction) => (
                  <li key={instruction.instruction}>
                    {instruction.instruction}
                  </li>
                ))}
              </ol>
            </div>
          </div>

          {/* Nav between drinks */}
          <div className="mt-12 flex items-center justify-between">
            <div className="flex w-10 justify-center rounded-full border py-2">
              <IoMdArrowRoundBack size={20} />
            </div>
            <Link to="/" className="rounded-full border px-3 py-1 font-medium">
              Home
            </Link>
            <div className="flex w-10 justify-center rounded-full border py-2">
              <IoMdArrowRoundForward size={20} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DrinkDetailAPI;
