import { useForm, SubmitHandler, useFieldArray } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { ErrorMessage } from "@hookform/error-message";
import { Button } from "@/components/ui/Button";
import {
  inputDivStyles,
  inputStyles,
  labelStyles,
} from "@/utils/tailwindStyles";
import { DrinkFormInputs } from "@/utils/types";
import { useCreateDrink } from "@/features/drinks/useCreateDrink";
import { IoMdArrowRoundBack } from "react-icons/io";
import { addFakeDrink } from "@/features/drinks/fakeDrinkSlice";
import { selectIsLoggedIn } from "@/features/auth/authSlice";

function CreateDrink() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  // Check if Logged In
  const isLoggedIn = useSelector(selectIsLoggedIn);

  const {
    register,
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<DrinkFormInputs>({
    defaultValues: {
      drinkIngredients: [{ name: "", type: "", quantity: "", units: "" }],
      drinkInstructions: [{ orderNum: null, instruction: "" }],
    },
  });

  const {
    fields: ingredientFields,
    append: ingredientAppend,
    remove: ingredientRemove,
  } = useFieldArray({ control, name: "drinkIngredients" });

  const {
    fields: instructionFields,
    append: instructionAppend,
    remove: instructionRemove,
  } = useFieldArray({ control, name: "drinkInstructions" });

  const { isPending, createNewDrink } = useCreateDrink();

  const onSubmit: SubmitHandler<DrinkFormInputs> = (drinkData) => {
    // Filter out empty ingredients or instructions
    const validIngredients = drinkData.drinkIngredients.filter((ingredient) => {
      return ingredient.name || ingredient.quantity || ingredient.units;
    });

    const validInstructions = drinkData.drinkInstructions.filter(
      (instruction) => {
        return instruction.orderNum || instruction.instruction;
      },
    );

    if (isLoggedIn) {
      const formData = new FormData();
      formData.append("name", drinkData.drinkName);
      formData.append("image", drinkData.drinkImage[0]);
      formData.append("description", drinkData.drinkDescription);
      formData.append("ingredients", JSON.stringify(validIngredients));
      formData.append("instructions", JSON.stringify(validInstructions));

      createNewDrink(formData);
    } else {
      const fakeDrinkData = {
        _id: Math.floor(Math.random() * 10000000),
        name: drinkData.drinkName,
        description: drinkData.drinkDescription,
        ingredients: validIngredients,
        instructions: validInstructions,
      };

      dispatch(addFakeDrink(fakeDrinkData));
      navigate("/");
    }
  };

  return (
    <div className="mx-auto mb-14 mt-10 max-w-[35rem] px-8">
      <div className="flex items-center justify-between">
        <button
          onClick={() => navigate(-1)}
          className="rounded-full border-2 px-3 py-2"
        >
          <IoMdArrowRoundBack size={25} />
        </button>
      </div>
      <h1 className="text-center text-xl font-semibold md:text-3xl">
        New Drink
      </h1>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="flex flex-col items-start justify-center gap-y-8"
      >
        <div className={inputDivStyles}>
          <label className={labelStyles} htmlFor="drinkName">
            Name
          </label>
          <input
            id="drinkName"
            type="text"
            className={inputStyles}
            disabled={isPending}
            {...register("drinkName", { required: "Drink Name is Required" })}
          />
          <ErrorMessage
            errors={errors}
            name="drinkName"
            render={({ message }) => (
              <div className="absolute top-8 text-nowrap px-4 py-2 text-sm text-red-700">
                {message}
              </div>
            )}
          />
        </div>

        {isLoggedIn && (
          <div className={inputDivStyles}>
            <label className={labelStyles} htmlFor="drinkImage">
              Image
            </label>
            <input
              id="drinkImage"
              type="file"
              accept="image/*"
              className={inputStyles}
              disabled={isPending}
              {...register("drinkImage")}
            />
            <ErrorMessage
              errors={errors}
              name="drinkImage"
              render={({ message }) => (
                <div className="absolute top-8 text-nowrap px-4 py-2 text-sm text-red-700">
                  {message}
                </div>
              )}
            />
          </div>
        )}

        <div className={inputDivStyles}>
          <label className={labelStyles} htmlFor="drinkDescription">
            Description
          </label>
          <textarea
            id="drinkDescription"
            className={inputStyles}
            disabled={isPending}
            {...register("drinkDescription")}
          />
        </div>

        <div className="w-full">
          <label className={labelStyles}>Ingredients</label>
          <div className="mt-4 flex flex-col gap-y-1">
            <div className="mb-1 flex gap-3 text-sm font-semibold">
              <h3 className="w-1/4">Name</h3>
              <h3 className="w-1/4">Type</h3>
              <h3 className="w-1/4">Quantity</h3>
              <h3 className="w-1/4">Units</h3>
              <h3 className="w-10"></h3>
            </div>

            {ingredientFields.map((field, index) => (
              <div key={field.id} className="flex gap-2">
                {/* <input
                  type="hidden"
                  {...register(`drinkIngredients.${index}._id`)}
                /> */}
                <input
                  type="text"
                  className={inputStyles}
                  {...register(`drinkIngredients.${index}.name`)}
                  placeholder="Ingredient Name"
                />

                <input
                  type="text"
                  className={inputStyles}
                  {...register(`drinkIngredients.${index}.type`)}
                  placeholder="Type"
                />
                <input
                  type="text"
                  className={inputStyles}
                  {...register(`drinkIngredients.${index}.quantity`)}
                  placeholder="Quantity"
                />
                <input
                  type="text"
                  className={inputStyles}
                  {...register(`drinkIngredients.${index}.units`)}
                  placeholder="Units"
                />
                <Button
                  type="button"
                  variant="destructive"
                  onClick={() => {
                    ingredientRemove(index);
                  }}
                >
                  -
                </Button>
              </div>
            ))}
            <Button
              type="button"
              className="mt-3"
              onClick={() =>
                ingredientAppend({
                  name: "",
                  type: "",
                  quantity: "",
                  units: "",
                })
              }
            >
              Add Ingredient (+)
            </Button>
          </div>
        </div>

        <div className="w-full">
          <label className={labelStyles} htmlFor="drinkInstructions">
            Instructions
          </label>
          <div className="mt-4 flex flex-col">
            <div className="mb-1 flex gap-3 text-sm font-semibold">
              <h3 className="w-8 px-3">#</h3>
              <h3 className="pl-1">Instruction</h3>
            </div>
            {instructionFields.map((field, index) => (
              <div className="flex items-center gap-2" key={field.id}>
                {/* <input
                  type="hidden"
                  {...register(`drinkInstructions.${index}.id`)}
                /> */}
                <input
                  type="text"
                  className="w-8 rounded-md border p-3 text-center text-sm"
                  {...register(`drinkInstructions.${index}.orderNum`)}
                  placeholder="Order #"
                />
                <textarea
                  id="drinkInstructions"
                  // className={inputStyles}
                  className="w-full rounded-md border px-3 pt-3 text-sm"
                  {...register(`drinkInstructions.${index}.instruction`)}
                />
                <Button
                  type="button"
                  variant="destructive"
                  onClick={() => {
                    instructionRemove(index);
                  }}
                >
                  -
                </Button>
              </div>
            ))}
            <Button
              type="button"
              className="mt-3"
              onClick={() => {
                instructionAppend({
                  orderNum: null,
                  instruction: "",
                });
              }}
            >
              Add Instruction (+)
            </Button>
          </div>
        </div>

        <div className="mt-1 flex w-full justify-end">
          <Button>Add Drink</Button>
        </div>
      </form>
    </div>
  );
}

export default CreateDrink;
