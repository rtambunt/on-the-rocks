import { SubmitHandler, useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";

import { Button } from "../ui/Button";
import { inputStyles } from "@/utils/tailwindStyles";
import { UpdateUserData, User } from "@/utils/types";
import useUpdateUser from "@/features/user/useUpdateUser";

type EditUserFormProps = {
  curUser: User;
  setUserEditTrue: (newVal: boolean) => void;
};

function EditUserForm({ curUser, setUserEditTrue }: EditUserFormProps) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      firstName: curUser.firstName,
      lastName: curUser?.lastName,
      email: curUser.email,
    },
  });
  const { updateUser } = useUpdateUser();

  const handleUpdateUserSubmit: SubmitHandler<UpdateUserData> = (userData) => {
    updateUser(userData);
    setUserEditTrue(false);
  };

  // -- Tailwind Styles --
  const errorStyles =
    "absolute -right-80 top-0 text-nowrap text-sm text-red-700";

  return (
    <form
      onSubmit={handleSubmit(handleUpdateUserSubmit)}
      className="rounded-lg bg-gray-100 px-16 py-12"
    >
      <div className="mb-5 flex items-center justify-between">
        <h2 className="text-2xl font-semibold ">Edit Account Info</h2>
        <Button>Save Changes</Button>
      </div>

      <div className="ml-8 grid grid-cols-2 gap-y-1">
        <label htmlFor="firstName" className="font-semibold">
          First Name:
        </label>
        <div className="relative">
          <input
            id="firstName"
            type="text"
            className={inputStyles}
            {...register("firstName", {
              required: "First name field is required!",
            })}
          />
          <ErrorMessage
            errors={errors}
            name="firstName"
            render={({ message }) => (
              <div className={errorStyles}>{message}</div>
            )}
          />
        </div>

        <label htmlFor="lastName" className="font-semibold">
          Last Name:
        </label>
        <input
          id="lastName"
          type="text"
          className={inputStyles}
          {...register("lastName")}
        />

        <label htmlFor="email" className="font-semibold">
          Email:
        </label>
        <div className="relative">
          <input
            id="email"
            type="text"
            className={inputStyles}
            {...register("email", {
              required: "Email field is required!",
            })}
          />
          <ErrorMessage
            errors={errors}
            name="email"
            render={({ message }) => (
              <div className={errorStyles}>{message}</div>
            )}
          />
        </div>
      </div>
    </form>
  );
}

export default EditUserForm;
