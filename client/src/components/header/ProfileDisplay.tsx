// import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { IoLogOutOutline } from "react-icons/io5";
import { FaUser } from "react-icons/fa";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/DropdownMenu";

import { useLogout } from "@/features/auth/useLogout";
import { selectInitials } from "@/features/auth/authSlice";
// import { useGetMe } from "@/features/auth/useGetMe";
// import { getUserInitials } from "@/utils/utils";

function ProfileDisplay() {
  // const [initials, setInitials] = useState<string>();

  const navigate = useNavigate();

  const initials = useSelector(selectInitials);
  const { logout } = useLogout();

  // const { curUser } = useGetMe();

  // useEffect(() => {
  //   setInitials(getUserInitials(curUser.firstName, curUser?.lastName));
  // }, [curUser]);

  return (
    <DropdownMenu>
      <DropdownMenuTrigger>
        <button className="flex h-12 w-12 items-center justify-center rounded-full border-2">
          {initials}
        </button>
      </DropdownMenuTrigger>
      <DropdownMenuContent className="mx-auto mr-4 w-52 ">
        <DropdownMenuLabel className="text-center">
          My Account
        </DropdownMenuLabel>
        <DropdownMenuSeparator />
        <DropdownMenuItem
          className="flex cursor-pointer gap-x-8"
          onClick={() => navigate("/profile")}
        >
          <FaUser /> Profile
        </DropdownMenuItem>
        <DropdownMenuItem
          className="flex cursor-pointer gap-x-8"
          onClick={() => logout()}
        >
          <IoLogOutOutline /> Logout
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

export default ProfileDisplay;
