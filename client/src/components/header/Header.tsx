import { NavLink, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

import { Button } from "../ui/Button";
import { selectIsLoggedIn } from "@/features/auth/authSlice";
import ProfileDisplay from "./ProfileDisplay";

function Header() {
  const isLoggedIn = useSelector(selectIsLoggedIn);

  const navigate = useNavigate();

  return (
    <div className="relative flex items-center justify-between px-4 pt-4 text-amber-950">
      <NavLink to="/">
        <h1 className="text-2xl font-semibold uppercase">On the Rocks</h1>
      </NavLink>

      {isLoggedIn ? (
        <ProfileDisplay />
      ) : (
        <Button onClick={() => navigate("/sign-in")} variant="secondary">
          Sign In
        </Button>
      )}
    </div>
  );
}

export default Header;
