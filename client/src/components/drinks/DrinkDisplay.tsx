import { Link, useNavigate } from "react-router-dom";
import { RxCross2 } from "react-icons/rx";
import { BiDrink } from "react-icons/bi";
import { useDispatch, useSelector } from "react-redux";

import type { Drink } from "@/utils/types";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "../ui/Dialog";
import { Button } from "../ui/Button";
import { DialogClose } from "@radix-ui/react-dialog";
import { useDeleteDrink } from "@/features/drinks/useDeleteDrink";
import { selectIsLoggedIn } from "@/features/auth/authSlice";
import { deleteFakeDrink } from "@/features/drinks/fakeDrinkSlice";

type Props = {
  drink: Drink;
  editMode: boolean;
};

function DrinkDisplay({ drink, editMode }: Props) {
  const isLoggedIn = useSelector(selectIsLoggedIn);
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const { deleteDrink } = useDeleteDrink();

  return (
    <div className="relative">
      {editMode && (
        <div className="absolute -right-4 -top-3 ">
          <Dialog>
            <DialogTrigger>
              <button className="flex items-center justify-center rounded-full border bg-red-400 p-2 text-slate-50">
                <RxCross2 size={17} />
              </button>
            </DialogTrigger>
            <DialogContent>
              <DialogHeader>
                <DialogTitle>Are you absolutely sure?</DialogTitle>
                <DialogDescription>
                  <div>
                    This action cannot be undone. This will{" "}
                    <span className="font-bold">permanently delete</span>
                    your drink and remove your data from our servers.
                  </div>
                </DialogDescription>
              </DialogHeader>
              <div className="flex justify-end gap-x-2">
                <Button
                  onClick={() => {
                    if (isLoggedIn) {
                      deleteDrink({ drinkId: drink._id });
                    } else {
                      dispatch(deleteFakeDrink(drink._id));
                      navigate(0);
                    }
                  }}
                  variant="destructive"
                >
                  Yes, delete this drink
                </Button>
                <DialogClose>
                  <Button>Close</Button>
                </DialogClose>
              </div>
            </DialogContent>
          </Dialog>
        </div>
      )}

      <Link to={`/drink/${drink._id}`}>
        <div className="flex h-full flex-col justify-between gap-y-6 rounded-lg border bg-background_dark px-6 pb-4 pt-5 shadow-sm shadow-background_dark">
          {drink?.image ? (
            <img
              className="m-auto w-52 rounded-md"
              src={drink.image}
              alt={drink.name + "Photo"}
            />
          ) : (
            <div className="flex h-full w-full  items-center justify-center rounded-lg border py-5">
              <BiDrink className="h-3/4 w-3/4" />
            </div>
          )}

          <h3 className="text-lg font-semibold md:text-xl">{drink.name}</h3>
        </div>
      </Link>
    </div>
  );
}

export default DrinkDisplay;
