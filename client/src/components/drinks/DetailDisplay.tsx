import { Link } from "react-router-dom";

import type { Drink, Ingredient, Instruction } from "@/utils/types";

type Props = {
  curDrink: Drink;
};

function DetailDisplay({ curDrink }: Props) {
  // --- Tailwind classes ---
  const heading2 = "mt-2 text-lg font-semibold";

  return (
    <div>
      <h1 className="mt-6 text-center text-3xl font-semibold">
        {curDrink.name}
      </h1>
      <img
        src={curDrink.image}
        alt={curDrink.name + "Photo"}
        className="mx-auto mt-10 w-56 rounded-md border"
      />

      <div className="mt-6 flex flex-col gap-y-6">
        <div>
          <h2 className={heading2}>Description</h2>
          <hr />
          <p className="mt-3">{curDrink.description}</p>
        </div>

        <div className="border-4 border-primary bg-background_dark px-8 pb-6 pt-3">
          <h2 className={heading2}>Ingredients</h2>
          <hr />
          <ul className=" list-inside list-disc pl-4">
            {curDrink.ingredients.map((ingredient: Ingredient) => (
              <li key={ingredient.name}>{ingredient.name}</li>
            ))}
          </ul>
        </div>

        <div>
          <h2 className={heading2}>Quantities</h2>
          <hr />
          <ul className="mt-3 list-inside list-disc pl-4">
            <li>Item 1: 1.5oz</li>
            <li>Item 2: 1tbsp</li>
          </ul>
        </div>

        <div>
          <h2 className={heading2}>Instructions</h2>
          <hr />
          <ol className="mt-3 list-inside list-decimal pl-4">
            {curDrink.instructions.map((instruction: Instruction) => (
              <li key={instruction.instruction}>{instruction.instruction}</li>
            ))}
          </ol>
        </div>
      </div>

      {/* Nav between drinks */}
      <div className="mt-12 flex items-center justify-between">
        {/* <div className="flex w-10 justify-center rounded-full border py-2">
          <IoMdArrowRoundBack size={20} />
        </div> */}
        <Link to="/" className="rounded-full border px-3 py-1 font-medium">
          Home
        </Link>
        {/* <div className="flex w-10 justify-center rounded-full border py-2">
          <IoMdArrowRoundForward size={20} />
        </div> */}
      </div>
    </div>
  );
}

export default DetailDisplay;
