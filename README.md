<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://www.ontherocksapp.com">
    <img src="./client/public/on-the-rocks-logo.png" alt="On-the-Rocks-Logo" width="80" height="80">
  </a>

  <h2 align="center">On the Rocks</h2>

  <p align="center">
    An app to store all of your favorite bartending drink recipes!
    <br />
  </p>
  <a href="https://www.ontherocksapp.com"><strong>Live site here »</strong></a>

</div>
 <br  />
 <br  />

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contact">Contact + Project Links</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

On the Rocks is a passion project built to help people start their bartending journey! Create and edit your favorite drink recipes to keep track of your personal bartender menu

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Built With

#### Frontend

- React
- TypeScript
- Redux Toolkit
- Tanstack Query
- React Router
- Tailwind

#### Backend

- Node
- Express
- MongoDB
- Google Cloud Storage
- Sendgrid

#### Deployment

- Vercel - Frontend Hosting
- Render - Backend Web Service

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

**Sign up:** <br/>

(!! You may still use most app features without signing up) <br/>

1. Navigate to [ontherocksapp.com](https://ontherocksapp.com)
2. If you wish to save your drinks across devices, click on the **Sign In** button
3. On the Login page, click **"Don't have an account? Sign up here!"** highlighted in blue at the bottom
4. Fill in account info and click **Create Account**
5. Check your email inbox and verify account using the On the Rocks confirmation email that was sent
6. Your account is created!

**Adding a drink:**

1. Click on the **Add Drink** button at the top of the homepage
2. Fill out drink info (drink name is required!)
3. Click **Add Drink** button at the bottom of the form

**Editing a drink:**

1. Click on the drink you would like to edit in the homepage
2. This will take you to the drink detail page. Click on the **edit drink pencil icon** at the top right.
3. Fill out new drink info and click **Save Changes** when finished
4. Click on the **Delete Drink** button instead if you wish to delete the drink

**Deleting a drink:**

1. Click on the **edit drink pencil icon** at the top of the homepage
2. Red 'x' icons will appear above each drink. Click on them if you wish to delete that specific drink

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ROADMAP -->

## Roadmap

- [ ] Add GPT drink recommendations
- [ ] Add search bar + filter drinks feature

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTACT -->

## Contact + Project Links

Robbie Tambunting

Website: [www.robbietambunting.com](https://www.robbietambunting.com) <br/>
LinkedIn: [https://www.linkedin.com/in/robbie-tambunting/](https://www.linkedin.com/in/robbie-tambunting/) <br/>

Live Site: [www.ontherocksapp.com](https://www.ontherocksapp.com) <br/>
Project Repo: [On the Rocks Gitlab Repo](https://gitlab.com/rtambunt/on-the-rocks)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS -->

## Acknowledgments
